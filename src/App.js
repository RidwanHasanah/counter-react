import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React Pondok IT</h1>
        </header>
        <p className="App-intro">
        <Counter x="Counter"/>
        <Counter x="Counter 1"/>
        <Counter x="Counter 2"/>
        </p>
      </div>
    );
  }
}

export default App;

class Counter extends Component{

  state = {
    number : 0
  }

  add = () =>{
    if(this.state.number >=10){
      alert('maaf kaka udah 10 g bisa lebih dari 10')
    }else{
      this.setState({
        number : this.state.number + 1
      })
    }
    
  }

  substract = () =>{
    if(this.state.number <= 0){
      alert("Maaf bang oki g boleh kurang dari NOL")
    }else{
      this.setState({
        number : this.state.number - 1
      })
    }
    
  }
  

  render(){
    return(
      <div>
        <h1>{this.props.x}</h1>
        <br/>
        <h2>{this.state.number}</h2>
        <button onClick={this.add} >ADD</button><button onClick={this.substract} >SUBSTRACT</button>
      </div>
    )
  }
}